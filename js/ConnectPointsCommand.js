import Command from './Command.js';
import Line from "./lib/Line.js";

export default class ConnectPointsCommand extends Command {
    /**
     *
     * @param parameters
     */
    constructor(parameters) {
        let {from, to, path} = parameters;
        super();
        this.from = from;
        this.to = to;
        this.path = path;
        this.container = this.path.querySelector('.path__lines');
    }

    do() {
        this.line = new Line(this.container, this.from.getPosition(), this.to.getPosition());
        this.line.bindPoints(this.from, this.to);
        return this.line;
    }

    undo() {
        this.line.destroy();
        this.line.unbindPoints(this.from, this.to);
        // disconnectPoints(this.from, this.to);
        super.undo();
    }
}