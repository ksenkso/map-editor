export default class UndoRedoStack {
    constructor() {
        this.undoStack = [];
        this.redoStack = [];
    }
    do(command) {
        const result = command.do();
        this.undoStack.push(command);
        return result;
    }
    undo() {
        if (this.undoStack.length) {
            const command = this.undoStack.pop();
            const result = command.undo();
            this.redoStack.push(command);
            return result;
        }
    }
    redo() {
        if (this.redoStack.length) {
            const command = this.redoStack.pop();
            const result = command.do();
            this.undoStack.push(command);
            return result;
        }
    }
}