export default class Command {
    constructor() {
        this.isDone = false;
        this.commands = [];
    }

    /**
     * Executes all the sub-commands and marks current command as done.
     *
     * @return {void}
     */
    do() {
        this.commands.forEach(command => command.do());
        this.isDone = true;
    }

    /**
     * Reverts all the sub-commands and marks current command as undone.
     *
     * @return {void}
     */
    undo() {
        this.commands.forEach(command => command.undo());
        this.isDone = false;
    }
}