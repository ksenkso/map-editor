import UndoRedoStack from "./UndoRedoStack.js";
import Selection from "./lib/Selection.js";
const loader = document.querySelector('.loader');
const svg = document.querySelector('svg');
const polylines = Array.from(document.querySelectorAll('[data-name="obj"] polyline'));
const SVGNamespace = 'http://www.w3.org/2000/svg';
const activePath = document.createElementNS(SVGNamespace, 'g');
activePath.classList.add('path');
const pointsContainer = document.createElementNS(SVGNamespace, 'g');
pointsContainer.classList.add('path__points');
const linesContainer = document.createElementNS(SVGNamespace, 'g');
linesContainer.classList.add('path__lines');
activePath.appendChild(linesContainer);
activePath.appendChild(pointsContainer);
svg.appendChild(activePath);
const G = {
    index: -1
};
const GLOBALS = {
    svg,
    SVGNamespace,
    polylines,
    pointsContainer,
    linesContainer,
    G,
    activePath,
    loader,
    points: [],
    currentPoint: null,
    prevPoint: null,
    currentObject: null,
    commandManager: new UndoRedoStack(),
    selection: new Selection(),
    draggable: null,
    IS_MOUSE_DOWN: false,
    IS_DRAGGING: false
};
export default GLOBALS;
window.GLOBALS = GLOBALS;