import GLOBALS from './globals.js';
import {
    calculateNormals,
    createPath,
    drawGraph,
    getAdjacencyList,
    getDoorCoords,
    getIntersectionPoint,
    getMouseCoords
} from './lib/utils.js';
import PointCommand from './PointCommand.js';
import './lib/dragndrop.js';
import Point from "./lib/Point.js";
import ConnectPointsCommand from "./ConnectPointsCommand.js";
// import './lib/components/Loader.js';
//region elements
const mapFileInput = document.querySelector('.js-map-file-input');

const showPointsButton = document.querySelector('.js-show-points');
//endregion
//region events

GLOBALS.svg.addEventListener('click', onSvgClick);
document.addEventListener('keyup', onKeyUp);
document.addEventListener('keypress', onKeyPress);
showPointsButton.addEventListener('click', showCalculatedPath);
mapFileInput.addEventListener('change', async e => {
    GLOBALS.loader.classList.add('loader_active');
    await onMapFileChange(e);
    GLOBALS.loader.classList.remove('loader_active');

});
//TODO: implement real file checking
function checkMapFile(file) {
    return true;
}

/**
 *
 * @param {File} file
 * @return {Promise<String>}
 */
async function readFile(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onerror = function (error) {
            reject(error);
        };
        reader.onloadend = function () {
            if (reader.readyState === FileReader.DONE && reader.result) {
                resolve(reader.result);
            } else {
                const message = `State: ${reader.readyState}, result: ${reader.result}, reader: ${reader}`;
                const error = new Error(message);
                reject(error);
            }
        };
        reader.readAsText(file);
    });

}

async function onMapFileChange(e) {
    const file = e.target.files[0];
    if (file) {
        if (checkMapFile(file)) {
            const svgText = await readFile(file);
            const domParser = new DOMParser();
            const svgDocument = domParser.parseFromString(svgText, 'image/svg+xml');
            const walker = svgDocument.createTreeWalker(svgDocument.rootElement, NodeFilter.SHOW_ELEMENT);
            while (walker.nextNode()) {
                walker.currentNode.removeAttribute('onload');
            }
            document.querySelector('.map').appendChild(svgDocument.rootElement);
        } else {

        }
    }
}

function onSvgClick(e) {
    // console.log('svg click');

    const {x, y} = getMouseCoords(e);
    // Always create a new point when SVG id clicked
    if (!GLOBALS.activePath) {
        GLOBALS.activePath = createPath();
        GLOBALS.svg.appendChild(GLOBALS.activePath);
    }
    GLOBALS.bufferCommand = new PointCommand({point: {x, y}, path: GLOBALS.activePath});
    const point = GLOBALS.commandManager.do(GLOBALS.bufferCommand);
    GLOBALS.points.push(point);
}

function onKeyUp(e) {
    if (e.key === 'Delete') {
        if (GLOBALS.selection.elements.length) {
            GLOBALS.selection.elements.forEach(element => element.destroy())
        }
    }

}

function onKeyPress(e) {
    if (e.ctrlKey) {
        switch (e.code) {
            case 'KeyZ': {
                GLOBALS.commandManager.undo();
                break;
            }
            case 'KeyY': {
                GLOBALS.commandManager.redo();
                break;
            }
        }
    } else {

        switch (e.key) {
            case 'Delete': {
                if (GLOBALS.currentObject) {
                    GLOBALS.currentObject.destroy();
                }
            }
        }
    }
}

function showCalculatedPath() {

    let points = GLOBALS.polylines.map(getDoorCoords);
    const firstPointPosition = points[0];
    // Sort points by distance to the start of the path (ascending)
    const sortable = points.slice(1).sort((a, b) => {
        const distanceA = Point.distance(firstPointPosition, a);
        const distanceB = Point.distance(firstPointPosition, b);
        return distanceA - distanceB;
    });
    /**
     *
     * @type {{x: number, y: number, p: TPoint[], id: String}[]}
     */
    points = [points[0], ...sortable];
    let normals = calculateNormals(points);
    showPoints(normals);
}
//endregion
//region graph

function showPoints(normals) {
    //TODO: Try to keep track of the previous point instead of keeping in memory whole list of points
    const newPoints = [];
    let prevIntersectionPoint;
    // Go through all door normals on the map
    const linesContainer = Array.from(GLOBALS.activePath.querySelector('.path__lines').children);
    const handDrawnPath = GLOBALS.activePath;
    GLOBALS.activePath = createPath();
    GLOBALS.svg.appendChild(GLOBALS.activePath);
    normals.forEach(({linePoints, x, y, id}) => {
        let intersection;
        // Set a point at the center of the door
        const center = GLOBALS.commandManager.do(new PointCommand({point: {x, y}, connectCurrent: false, path: GLOBALS.activePath}));
        center.id = id;
        newPoints.push(center);
        let currentDistance;
        // For each normal go through all path segments
        linesContainer.forEach(line => {
            // Get coords of the ends of each segment
            const points = [
                {x: +line.getAttribute('x1'), y: +line.getAttribute('y1')},
                {x: +line.getAttribute('x2'), y: +line.getAttribute('y2')}
            ];
            // Get the intersection point of the normal and current path segment
            const point = getIntersectionPoint(...linePoints, ...points);
            // If intersection point exists
            if (point) {
                // Check the distance from it to the center of the door.
                // We should only remember the closest point
                const distance = Point.distance({x, y}, point);
                // If there is no closest point yet
                // or distance for new point is less the that for previous point
                if (!currentDistance || currentDistance > distance) {
                    currentDistance = distance;
                    // Set this point as the closest intersection point
                    intersection = point;
                }
            }
        });
        // If the intersection point found
        if (intersection) {
            // Try to connect it with the center of the door
            const point = GLOBALS.commandManager.do(new PointCommand({point: intersection, connectCurrent: true, path: GLOBALS.activePath}));
            if (prevIntersectionPoint) {
                console.log(GLOBALS.activePath);
                GLOBALS.commandManager.do(new ConnectPointsCommand({from: prevIntersectionPoint, to: point, path: GLOBALS.activePath}));
            }
            newPoints.push(point);
            prevIntersectionPoint = point;
        }
    });
    handDrawnPath.style.visibility = 'hidden';
    // TODO: Try drawing this list
    return getAdjacencyList(newPoints);
}

function drawFromLocalStorage () {
    const list = JSON.parse(localStorage.getItem('list'));
    console.log(list);
    GLOBALS.activePath = createPath();
    GLOBALS.svg.appendChild(GLOBALS.activePath);
    drawGraph(list);
}
//endregion
//region GlobalExports
window.drawFromLocalStorage = drawFromLocalStorage;
window.getAdjacencyList = getAdjacencyList;
window.drawGraph = drawGraph;
//endregion
