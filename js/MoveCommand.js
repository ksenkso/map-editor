/**
 * @typedef {{x: number, y: number}} TPoint
 */
import Command from "./Command.js";
import Vector from "./lib/Vector.js";

/**
 * @class MoveCommand
 * @property {Vector} delta
 */
export default class MoveCommand extends Command {
    /**
     *
     * @param {Point} point
     * @param {TPoint} [from]
     * @param {TPoint} [to]
     */
    constructor(point, from, to) {
        super();
        this.point = point;
        this.from = from;
        this.to = to;
    }
    do() {
        /*this.point.setAttribute('cx', this.to.x);
        this.point.setAttribute('cy', this.to.y);*/
        this.point.setPosition(this.to);
        if (this.point.from.length) {
            this.point.from.forEach(line => line.setCoords(this.to));
        }
        if (this.point.to.length) {
            this.point.to.forEach(line => line.setCoords(null, this.to));
        }
        super.do();
    }
    undo() {
        /*this.point.setAttribute('cx', this.from.x);
        this.point.setAttribute('cy', this.from.y);*/
        this.point.setPosition(this.from);
        if (this.point.from.length) {
            this.point.from.forEach(line => line.setCoords(this.from));
        }
        if (this.point.to.length) {
            this.point.to.forEach(line => line.setCoords(null, this.from));
        }
        super.undo();
    }
}
