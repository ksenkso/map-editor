export default class Selection {
    get current() {
        if (this.elements.length){
            return this.elements[this.elements.length-1];
        }
    }
    get previous() {
        if (this.elements.length > 1){
            return this.elements[this.elements.length-2];
        }
    }
    /**
     * @constructor
     */
    constructor() {
        /**
         *
         * @type {Primitive[]}
         */
        this.elements = [];
    }

    /**
     *
     * @param {Primitive} element
     */
    add(element) {
        element.isSelected = true;
        this.elements.push(element);

    }

    /**
     *
     * @param {Primitive} element
     */
    remove(element) {
        element.isSelected = false;
        this.elements.splice(this.elements.indexOf(element), 1);

    }

    /**
     *
     * @param {Primitive|Primitive[]} items
     */
    set(...items) {
        this.elements.forEach(element => element.isSelected = false);
        this.elements = Array.from(items).map(item => {
            item.isSelected = true;
            return item;
        });

    }
}