export default class Vector {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    /**
     *
     * @param v
     * @param scale
     * @returns {TPoint}
     */
    static scale(v, scale) {
        return {x: v.x * scale, y: v.y * scale};
    }

    /**
     *
     * @param v1
     * @param v2
     * @returns {{x: *, y: *}}
     */
    static add(v1, v2) {
        return {x: v1.x + v2.x, y: v1.y + v2.y};
    }

    /**
     *
     * @param v1
     * @param v2
     * @returns {TPoint}
     */
    static sub(v1, v2) {
        return {x: v1.x - v2.x, y: v1.y - v2.y};
    }

    /**
     *
     * @param from
     * @param to
     * @returns {TPoint}
     */
    static fromTo(from, to) {
        return {x: to.x - from.x, y: to.y - from.y};
    }

    /**
     *
     * @param v
     * @returns {number}
     */
    static length(v) {
        return Math.sqrt(v.x * v.x + v.y * v.y)
    }

    /**
     *
     * @param v1
     * @param v2
     * @returns {number}
     */
    static dot(v1, v2) {
        return v1.x * v2.x + v1.y * v2.y;
    }

    /**
     *
     * @param line
     * @returns {TPoint}
     */
    static fromSVGLine(line) {
        return Vector.fromTo(
            {x: +line.getAttribute('x1'), y: +line.getAttribute('y1')},
            {x: +line.getAttribute('x2'), y: +line.getAttribute('y2')}
        )
    }
}