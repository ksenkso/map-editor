/**
 * @class Primitive
 * @property {SVGElement} element
 */
import GLOBALS from "../globals.js";

export default class Primitive {

    /**
     * Algorithm:
     * 1. Set _isSelected to value;
     * 2. If element is already selected, deselect it;
     * 3. If SHIFT key wasn't pressed, wipe the selection
     * 4. If element is not selected yet, push it to the selection
     * @param value
     */
    set isSelected(value) {
        this._isSelected = value;
        if (value) {
            this.element.classList.add('primitive_active');
            // GLOBALS.selection.add(this);
        } else {
            // GLOBALS.selection.splice(GLOBALS.selection.indexOf(this));
            this.element.classList.remove('primitive_active');
        }
    }
    get isSelected() {
        return this._isSelected;
    }

    /**
     *
     * @param type
     * @return {SVGElement}
     */
    static createElement(type) {
        const element =  document.createElementNS(GLOBALS.SVGNamespace, type);
        element.classList.add('primitive');
        return element;
    }
    /**
     * @constructor
     * @param {SVGElement} container
     */
    constructor(container) {
        this.container = container;
        this._isSelected = false;
    }

    /**
     * Create an element, assign it to `this.element`.
     * @see Primitive
     */
    init() {
        this.element.ownerObject = this;
    }

    /**
     * Destructor of the vector primitive. Removes it from the SVG container.
     */
    destroy() {
        this.element.remove();
    }

    onClick(e) {
        if (e.shiftKey) {
            if (this.isSelected) {
                GLOBALS.selection.remove(this);
            } else {
                GLOBALS.selection.add(this);
            }
        } else {
            GLOBALS.selection.set(this);
        }

    }

    static resolveContainer(container, selector) {
        return container.querySelector(selector);
    }
}