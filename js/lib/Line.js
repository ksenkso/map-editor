import Primitive from "./Primitive.js";

export default class Line extends Primitive {
    /**
     *
     * @param {SVGElement} container
     * @param {TPoint} from
     * @param {TPoint} to
     */
    constructor(container, from, to) {
        super(container);
        this.from = from;
        this.to = to;
        this.init();
        this.container.appendChild(this.element);
    }
    init() {
        this.element = Primitive.createElement('line');
        this.element.classList.add('primitive_line');
        this.setCoords(this.from, this.to);
        this.element.addEventListener('click', this.onClick.bind(this));
        super.init();
    }

    /**
     *
     * @param {Point} from
     * @param {Point} to
     */
    bindPoints(from, to) {
        from.from.push(this);
        to.to.push(this);
        from.points.add(to);
        to.points.add(from);
    }
    unbindPoints(from, to) {
        from.from.splice(from.from.indexOf(this), 1);
        to.to.splice(to.to.indexOf(this), 1);
        from.points.delete(to);
        to.points.delete(from);
    }
    /**
     *
     * @param {?TPoint} [from]
     * @param {?TPoint} [to]
     */
    setCoords(from, to) {
        if (from) {
            this.element.setAttribute('x1', from.x);
            this.element.setAttribute('y1', from.y);
        }
        if (to) {
            this.element.setAttribute('x2', to.x);
            this.element.setAttribute('y2', to.y);
        }
    }
    destroy() {
        super.destroy();
    }

    onClick(e) {
        e.stopPropagation();
        super.onClick(e);
    }
}