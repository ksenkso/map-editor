import GLOBALS from '../globals.js';
import MoveCommand from '../MoveCommand.js';
import {getMouseCoords} from './utils.js';
import Vector from "./Vector.js";

export function onPointMouseDown(e) {
    GLOBALS.IS_MOUSE_DOWN = true;
    GLOBALS.draggable = e.target;
    const x = +e.target.getAttribute('cx');
    const y = +e.target.getAttribute('cy');
    GLOBALS.bufferCommand = new MoveCommand(GLOBALS.draggable, {x, y});
}

function onMouseMove(e) {
    /**
     * Check if there was a movement to prevent false positives.
     * There may be a bug with some touch-pads, when `mousemove` event is triggered by click.
     */
    if (GLOBALS.IS_MOUSE_DOWN && (e.movementX || e.movementY)) {
        console.log('mouse move');
        GLOBALS.IS_DRAGGING = true;
        const coords = Vector.add(getMouseCoords(e), GLOBALS.bufferCommand.delta);
        GLOBALS.draggable.setPosition(coords);
        if (GLOBALS.draggable.from.length) {
            GLOBALS.draggable.from.forEach(line => line.setCoords(coords));
        }
        if (GLOBALS.draggable.to.length) {
            GLOBALS.draggable.to.forEach(line => line.setCoords(null, coords));
        }
    }
}
function onMouseUp(e) {
    if (GLOBALS.IS_DRAGGING && GLOBALS.bufferCommand) {
        GLOBALS.bufferCommand.to = Vector.add(getMouseCoords(e), GLOBALS.bufferCommand.delta);
        GLOBALS.commandManager.do(GLOBALS.bufferCommand);
    }
    GLOBALS.IS_DRAGGING = false;
    GLOBALS.IS_MOUSE_DOWN = false;
    GLOBALS.draggable = null;
}

GLOBALS.svg.addEventListener('mousemove', onMouseMove);
GLOBALS.svg.addEventListener('mouseup', onMouseUp);