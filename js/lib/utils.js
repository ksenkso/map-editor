import GLOBALS from '../globals.js';
import PointCommand from '../PointCommand.js';
import Primitive from "./Primitive.js";
import Vector from "./Vector.js";

/**
 * @typedef {{location: TPoint, points: Number[], marked: Boolean}} TNode
 */
/**
 *
 * @return {number}
 */
export function getZoomLevel() {
    const viewBox = GLOBALS.svg.getAttribute('viewBox').split(' ');
    const bounds = GLOBALS.svg.getBoundingClientRect();
    return bounds.width / viewBox[2];
}

/**
 *
 * @param {MouseEvent} e
 * @returns {TPoint}
 */
export function getMouseCoords(e) {
    const zoomLevel = getZoomLevel();
    const bounds = GLOBALS.svg.getBoundingClientRect();
    const x = (e.clientX - bounds.left) / zoomLevel;
    const y = (e.clientY - bounds.top) / zoomLevel;
    return {x, y};
}
export function setScene(svg) {

}

/**
 *
 * @param list
 * @returns TNode[];
 */
export function getAdjacencyList(list) {
    return list.map((point) => {
        const points = [];
        point.points.forEach(v => {
            points.push(list.indexOf(v));
        });
        return {
            location: point.getPosition(),
            points
        };
    });
}

/**
 * Recursive algorithm for drawing a graph by its adjacency list
 *
 * @param {TNode[]} neighbourList
 * @param {Number} [index = 0]
 */
export function drawGraph(neighbourList, index = 0) {
    const point = neighbourList[index];
    // If point was already marked, leave it
    if (!point.marked) {
        // Put the point on the SVG and mark as seen
        point.marked = true;
        // This is the source point, which adjacent points will be traversed
        const currentPoint = GLOBALS.commandManager.do(new PointCommand({
            point: point.location,
            path: GLOBALS.activePath
        }));
        // Go through all adjacent points and draw them
        point.points.forEach(i => {
            drawGraph(neighbourList, i);
            // Set selection to the source point to draw edges properly. In other case they
            // will form a single broken line
            GLOBALS.selection.set(currentPoint);
        });

    }
}

/**
 *
 * @param {TPoint} a
 * @param {TPoint} b
 * @param {TPoint} c
 * @param {TPoint} d
 * @return {TPoint|null}
 */
export function getIntersectionPoint(a, b, c, d) {
    let point = {};
    point.x = -((a.x * b.y - b.x * a.y) * (d.x - c.x) - (c.x * d.y - d.x * c.y) * (b.x - a.x)) / ((a.y - b.y) * (d.x - c.x) - (c.y - d.y) * (b.x - a.x));
    point.y = ((c.y - d.y) * (-point.x) - (c.x * d.y - d.x * c.y)) / (d.x - c.x);
    if (isNaN(point.x) || isNaN(point.y)) {
        return null;
    }
    // If point is out of the bounding box of the first line
    if (point.x <= Math.min(c.x, d.x)
        || point.x >= Math.max(c.x, d.x)
        || point.y <= Math.min(c.y, d.y)
        || point.y >= Math.max(c.y, d.y)
    ) {
        return null;
    }

    return point;
}

/**
 *
 * @param {SVGPolylineElement} polyline
 * @returns {{x: number, y: number, p: TPoint[], id: String}}
 */
export function getDoorCoords(polyline) {
    const points = polyline.getAttribute('points').split(' ');
    /**
     * Coordinate at index `0` is the X coordinate of the first point.
     * So, coordinate at index `1` is the Y coordinate of the first point.
     * Last point coordinates are two last numbers in the `points` array.
     * First and last points define a segment, that is actually a door to the room.
     *
     * @type {TPoint[]}
     */
    const p = [
        {x: +points[0], y: +points[1]},
        {
            x: +points[points.length - 2], y: +points[points.length - 1]
        }];
    const x = p[0].x + (p[1].x - p[0].x) / 2;
    const y = +p[0].y + (p[1].y - p[0].y) / 2;
    return {x, y, p, id: polyline.parentElement.getAttribute('id')};
}

/**
 *
 * @return {SVGElement}
 */
export function createPath() {
    const path = Primitive.createElement('g');
    path.classList.add('primitive_path');
    const lines = Primitive.createElement('g');
    lines.classList.add('path__lines');
    path.appendChild(lines);
    const points = Primitive.createElement('g');
    points.classList.add('path__points');
    path.appendChild(points);
    return path;
}

/**
 *
 * @param {{x: Number, y: Number, p: TPoint[], id: String}} centers
 * @return {{x: Number, y: Number, linePoints: Vector[], id: String}}
 */
export function calculateNormals(centers) {
    return centers.map(({x, y, p, id}) => {
        // Rotate segment by 90 degrees
        const linePoints = [
            new Vector(-p[0].y, p[0].x),
            new Vector(-p[1].y, p[1].x)
        ];
        // Get coordinates of the center of created segment
        const cx = linePoints[0].x + (linePoints[1].x - linePoints[0].x) / 2;
        const cy = linePoints[0].y + (linePoints[1].y - linePoints[0].y) / 2;
        // Create vector from the center of original segment to the center of new segment
        let v = new Vector(x - cx, y - cy);
        linePoints[0] = Vector.add(linePoints[0], v);
        linePoints[1] = Vector.add(linePoints[1], v);
        // Create two vectors: one for the first coordinate, one for the second
        const v1 = Vector.fromTo({x, y}, linePoints[0]);
        const v2 = Vector.fromTo({x, y}, linePoints[1]);
        // Add these vectors respectively to points of the new segment and scale them
        // by some big number to draw the line, that will cross whole map
        const k = 100;
        linePoints[0] = Vector.add(linePoints[0], Vector.scale(v1, k));
        linePoints[1] = Vector.add(linePoints[1], Vector.scale(v2, k));
        return {x, y, linePoints, id};
    })
}

export async function loadComponentTemplate(name) {
    const url = `../../examples/${name}.html`;
    const link = document.createElement('link');
    link.setAttribute('href', url);
    link.setAttribute('rel', 'import');
    link.addEventListener('load', () => {
        // resolve()
    })
    document.appendChild(link);
}

/**
 * Works only for space-separated strings
 * @param {String} string
 */
export function toUpperCamelCase(string) {
    string
        .split(' ')
        .map(sub => {
            sub[0] = sub[0].toUpperCase();
            return sub;
        })
        .join('');
}