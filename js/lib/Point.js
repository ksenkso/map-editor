import Vector from "./Vector.js";
import Primitive from "./Primitive.js";
import GLOBALS from "../globals.js";
import {getMouseCoords} from "./utils.js";
import ConnectPointsCommand from "../ConnectPointsCommand.js";
import MoveCommand from "../MoveCommand.js";

export default class Point extends Primitive {
    static distance(p1, p2) {
        return Vector.length(Vector.fromTo(p1, p2));
    }
    constructor(container, path, center = {x: 0, y: 0}, radius = 2) {
        super(container);
        this.center = center;
        this.radius = radius;
        this.path = path;
        /**
         *
         * @type {Set<Point>}
         */
        this.points = new Set();
        /**
         *
         * @type {Line[]}
         */
        this.from = [];
        /**
         *
         * @type {Line[]}
         */
        this.to = [];
        this.init();
        this.container.appendChild(this.element);
    }

    init() {
        this.element = Primitive.createElement('circle');
        this.element.classList.add('primitive_point');
        this.element.setAttribute('r', this.radius);
        this.setPosition(this.center);
        this.element.addEventListener('mousedown', this.onPointMouseDown.bind(this));
        this.element.addEventListener('click', this.onClick.bind(this));
        super.init();
        // Register global event listeners
        // Listen to keyboard event
    }
    onClick(e) {
        e.stopPropagation();
        GLOBALS.activePath = this.path;
        const previousObject = GLOBALS.selection.current;
        if (previousObject && previousObject instanceof Point) {
            if (e.ctrlKey) {
                if (this.path !== previousObject.path) {
                    this.moveToPath(previousObject);
                }
                if (!previousObject.points.has(this)) {
                    GLOBALS.commandManager.do(new ConnectPointsCommand({from: previousObject, to: this, path: this.path}));
                }
            }
        }
        super.onClick(e);

    }

    moveToPath(sourceObject) {
        // `this.path` is the destination path, `sourceObject.path` is the source path
        sourceObject.points.forEach(point => point.path = this.path);
        const destinationPoints = this.path.querySelector('.path__points');
        Array.from(sourceObject.path.querySelector('.path__points').children).forEach(point => {
            destinationPoints.appendChild(point);
        });
        const destinationLines = this.path.querySelector('.path__lines');
        Array.from(sourceObject.path.querySelector('.path__lines').children).forEach(point => {
            destinationLines.appendChild(point);
        });
    }

    onPointMouseDown(e) {
        console.log('point down');
        // this.isActive = true;
        GLOBALS.IS_MOUSE_DOWN = true;
        GLOBALS.draggable = this;
        const position = this.getPosition();
        GLOBALS.bufferCommand = new MoveCommand(GLOBALS.draggable, position);
        GLOBALS.bufferCommand.delta = Vector.sub(position, getMouseCoords(e));
    }
    /**
     *
     * @param {TPoint} newPosition
     */
    setPosition(newPosition) {
        this.element.setAttribute('cx', newPosition.x);
        this.element.setAttribute('cy', newPosition.y);
    }

    /**
     *
     * @returns {TPoint}
     */
    getPosition() {
        return {x: +this.element.getAttribute('cx'), y: +this.element.getAttribute('cy')};
    }

    destroy() {
        super.destroy();
        this.from.forEach(line => line.destroy());
        this.to.forEach(line => line.destroy());
    }


}