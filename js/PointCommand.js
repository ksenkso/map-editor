import GLOBALS from './globals.js';
import Command from './Command.js';
import ConnectPointsCommand from './ConnectPointsCommand.js';
import Point from "./lib/Point.js";

export default class PointCommand extends Command {
    constructor(parameters) {
        let {path, point: coords, connectCurrent = true, radius = 1.5} = parameters;
        super();
        this.coords = coords;
        this.connectCurrent = connectCurrent;
        this.path = path;
        this.container = this.path.querySelector('.path__points');
        this.radius = radius;
        //    TODO: Switch all commands constructors to single-object-argument form
    }

    do() {
        this.point = new Point(this.container, this.path, this.coords, this.radius);
        const currentObject = GLOBALS.selection.current;
        if (currentObject && currentObject instanceof Point && this.connectCurrent) {
            // console.log(this.path);
            this.commands.push(new ConnectPointsCommand({from: currentObject, to: this.point, path: this.path}));
        }
        /**
         * 1. Remember previous point.
         * 2. Set previous object to current object
         * 3. Make newly created point current object
         */
        this.prevPoint = GLOBALS.prevObject;
        GLOBALS.prevObject = GLOBALS.selection.current;
        GLOBALS.selection.set(this.point);
        super.do();
        return this.point;
    }

    undo() {
        this.point.destroy();
        GLOBALS.prevObject = this.prevPoint;
        super.undo();
    }
}